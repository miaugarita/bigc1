
const mostrar = document.getElementById("btnmostrar");
const tablaAlumnos = document.getElementById("tabla-alumnos");
const tablaAlumnosCuerpo = document.getElementById("tabla-alumnos-cuerpo");

mostrarBtn.addEventListener("click", () => {
  axios.get("alumnos.json").then(res => {
    const alumnos = res.data;
    let promedioGeneral = 0;
    for (const alumno of alumnos) {
      const promedioAlumno = (alumno.matematicas + alumno.quimica + alumno.fisica + alumno.geografia) / 4;
      promedioGeneral += promedioAlumno;
      tablaAlumnosCuerpo.innerHTML += `
        <tr>
          <td>${alumno.id}</td>
          <td>${alumno.matricula}</td>
          <td>${alumno.nombre}</td>
          <td>${alumno.matematicas}</td>
          <td>${alumno.quimica}</td>
          <td>${alumno.fisica}</td>
          <td>${alumno.geografia}</td>
          <td>${promedioAlumno.toFixed(2)}</td>
        </tr>
      `;
    }
    promedioGeneral /= alumnos.length;
    tablaAlumnosCuerpo.innerHTML += `
      <tr>
        <td colspan="7">Promedio General</td>
        <td>${promedioGeneral.toFixed(2)}</td>
      </tr>
    `;
    tablaAlumnos.style.display = "block";
  });
});